# Projet NLP 2020

## Installation
The following packages are required: numpy, pandas, io, os, pdfminer, scikit-learn, spacy, mglearn, Levenshtein-python.
To use French models ("fr_core_news_sm" and "fr_core_news_md") in spacy, they need to be downloaded with the command : python -m spacy download fr_core_news_sm

## Sources
This website has inspired the code to extract text from the pdf: https://stackoverflow.com/questions/26748788/extraction-of-text-from-pdf-with-pdfminer-gives-multiple-copies
This website has inspired the code for the topic extraction method: https://blog.usejournal.com/nlp-for-topic-modeling-summarization-of-financial-documents-10-k-q-93070db96c1d